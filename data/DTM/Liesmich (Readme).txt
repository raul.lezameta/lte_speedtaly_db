*****************************************************************************
*        DIGITALES LiDAR GEL�NDEMODELL VON ITALIEN, Version 50 Meter        *
*****************************************************************************
* inklusive SAN MARINO, VATIKANSTADT, MALTA


* Zusammengestellt und resampled von Sonny
* E-Mail:  sonnyy7@gmail.com
* Website: http://data.opendataportal.at/dataset/dtm-italy
* Alternativ gibt es auf der Website das Gel�ndemodell auch noch in anderen Rasterweiten.

======================================================================================


Dieses digitale H�henmodell (hier ein digitales Gel�ndemodell DGM - im Gegensatz zu einem digitalen Oberfl�chenmodell DOM) verwendet zu einem Gutteil per pr�ziser LiDAR Technik erzeugte Gel�ndemodelle als Datengrundlage. Diese wurden mittels Airborne Laserscan (ALS)-Befliegungen vermessen. Deshalb der gro�e Vorteil dieses Modells: Die Abweichungen der Gel�ndeh�hen in diesem Modell im Vergleich zu den Laserh�hen der Quelldaten, speziell in bergigem und h�geligem Terrain, sind deutlich geringer als jene der verbreiteten SRTM-Kacheln.

Alle Einzel-Dateien wurden neben- bzw. �bereinander gelegt und mit 20 Meter (Version 20m) bzw. 50 Meter (Version 50m) Pixelabstand resampled.

Als Dateiformat wurde das relativ verbreitete GeoTIFF-Format gew�hlt. Zus�tzlich habe ich noch 2 passende Projektionsdateien .prj und .tfw beigef�gt


PROJEKTION und DATEIFORMAT
**************************

- Koordinatenreferenzsystem: EPSG:25832
- Geod�tisches Datum: ETRS89
- Koordinatensystem: UTM Zone 32
- Horizontale Aufl�sung: 20 Meter (Version 20m) bzw. 50 Meter (Version 50m)
- Vertikale Aufl�sung: 0.1 Meter
- Dateiformat: GeoTIFF, 32-bit Floating Point, vertical unit: Meter, compression: Deflate


QUELLEN und LIZENZ
******************

Dieses Gel�ndemodell ist OFFEN, FREI und KOSTENLOS verwendbar, unterliegt aber der LIZENZ: Creative Commons Namensnennung 4.0 (CC BY 4.0), siehe https://creativecommons.org/licenses/by/4.0/deed.de

Ihr d�rft daher dessen Daten vervielf�ltigen, verbreiten, �ffentlich zug�nglich machen, auch kommerziell nutzen, sowie Abwandlungen und Bearbeitungen des Inhalts anfertigen. Ihr solltet bei Verwendung meinen Namen (Sonny), sowie die Website (siehe ganz oben) erw�hnen, herzlichen Dank!


F�r dieses Gel�ndemodell wurden ausschlie�lich FREIE Opendata Quelldaten verwendet:


- Regione Abruzzo: DTM 10 m
http://opendata.regione.abruzzo.it/content/modello-digitale-del-terreno-risoluzione-10x10-metri

- Regione Basilicata: DTM 5 m
http://rsdi.regione.basilicata.it/dbgt-ctr/

- Regione Calabria: DTM 5 m
http://geoportale.regione.calabria.it/opendata

- Citta Metropolitana di Napoli: DTM 1 m
http://sit.cittametropolitana.na.it/lidar.html

- Regione Autonoma Friuli Venezia Giulia: DTM 10 m
http://irdat.regione.fvg.it/CTRN/ricerca-cartografia/

- Regione Lazio: DTM 5 m
http://dati.lazio.it/weblist/cartografia/prodotti/2002_2003_DTM_5mt_DXF/

- Regione Liguria: Modello Digitale del Terreno da CTR 1:5000 dal 2007 II ed. 3D/DB (= DTM 5 m)
http://www.regione.liguria.it/sep-servizi-online/catalogo-servizi-online/opendata/item/7192-dtm-modello-digitale-del-terreno-da-ctr-15000-dal-2007-ii-ed-3ddb-topografico-passo-5-m.html

- Regione Lombardia: DTM5x5 Modello Digitale del Terreno 2015 (= DTM 5 m)
http://www.geoportale.regione.lombardia.it/en/download-ricerca

- Regione Piemonte: RIPRESA AEREA ICE 2009-2011 - DTM (= DTM 5 m)
http://www.geoportale.piemonte.it/geocatalogorp/index.jsp

- Regione Puglia: DTM 8 m
http://www.sit.puglia.it/portal/portale_cartografie_tecniche_tematiche/Download/Cartografie

- Regione Autonoma della Sardegna, Sardegna Geoportale: DTM 1 m and DTM 10 m
http://www.sardegnageoportale.it/webgis2/sardegnamappe/?map=download_raster
DTM 1m, DTM MATTM 1m - Aree critiche, DTM MATTM 1m -  Costa Gallura, DTM MATTM 1m - Fasce fluviali, DTM 10m (digitized DTM based on a Topomap with 10 m contour lines)

- Regione Siciliana: MDT 2012-2013 2x2 ETRS89  (= DTM 2 m)
http://www.sitr.regione.sicilia.it/geoportalen/sfoglia-il-catalogo/

- Regione Toscana: DTM 10 m
http://dati.toscana.it/dataset/dem10mt

- Provincia Autonoma di Trento: LIDAR rilievo 2006/2007/2008 (= DTM 2 m)
http://dati.trentino.it/dataset/lidar-rilievo-2006-2007-2008-link-al-servizio-di-download

- Autonome Provinz Bozen-S�dtirol: DTM 5 m
http://www.provinz.bz.it/natur-raum/themen/landeskartografie-download.asp

- Regione Autonoma Valle d'Aosta: DTM 2005/2008 aggregato (voli laser scanner 2005 e 2008), intero territorio regionale (= DTM 2 m)
http://geoportale.partout.it/prodotti_cartografici/repertorio_cartografico/dtm/default_i.aspx

- Regione Veneto: DTM regionale con celle di 5 metri di lato (= DTM 5 m)
http://dati.veneto.it/dataset/dtm-regionale-con-celle-di-5-metri-di-lato

- Malta Planning Authority: DTM 10 m
https://www.pa.org.mt/

- Viewfinder Panoramas by Jonathan de Ferranti: 1" DTMs
http://viewfinderpanoramas.org/dem3.html
Jonathan de Ferranti erlaubte mir, seine H�hendaten f�r mein DGM unter den CC BY 4.0 Lizenzbedingungen zu verwenden.

- NASA: SRTM Version 3.0 Global 1" DTMs
http://dwtkns.com/srtm30m/


======================================================================================
======================================================================================



*************************************************************************
*        DIGITAL LiDAR TERRAIN MODEL OF ITALY, Version 50 meters        *
*************************************************************************
* including SAN MARINO, VATICAN CITY, MALTA


* Compiled and resampled by Sonny
* E-Mail:  sonnyy7@gmail.com
* Website: http://data.opendataportal.at/dataset/dtm-italy
* Alternatively you can also download the Terrain model with other Sample spaces from the Website.

======================================================================================

This Digital Elevation Model (exactly: Digital Terrain Model DTM - in contrast to Digital surface model DSM) is mostly based on precise LiDAR elevation sources. They have been surveyed using the method of Airborne Laserscan (ALS). The difference of my model's elevations, especially in rocky terrain, are significant less compared to those of the popular original SRTM-datasets.

The source files have been aranged next and above each other and resampled with a sample spacing of 20 meters (Version 20m) or 50 meters (Version 50m).

I used the relative widespread file format GeoTIFF. Additionally there are 2 projection files .prj and .tfw


PROJECTION and FILEFORMAT
*************************

- Coordinate Reference System: EPSG:25832
- Geodetic date: ETRS89
- Coordinate system: UTM Zone 32
- Horizontal resolution: 20 meters (Version 20m) or 50 meters (Version 50m)
- Vertical resolution: 0.1 meter
- Fileformat: GeoTIFF, 32-bit Floating Point, vertical unit: Meter, compression: Deflate


SOURCES und LICENCE
*******************

This Terrain Model is OPEN, FREE and WITHOUT CHARGE. But it is licensed using the following LICENCE: Creative Commons Attribution 4.0 (CC BY 4.0),
look at https://creativecommons.org/licenses/by/4.0/deed.en

You are allowed to copy, redistribute the material in any medium or format as well as remix, transform, and build upon the material for any purpose, even commercially. Please mention my name (Sonny) and the Website (see at the top), thank you!


I exclusively used FREE Opendata sources:


- Regione Abruzzo: DTM 10 m
http://opendata.regione.abruzzo.it/content/modello-digitale-del-terreno-risoluzione-10x10-metri

- Regione Basilicata: DTM 5 m
http://rsdi.regione.basilicata.it/dbgt-ctr/

- Regione Calabria: DTM 5 m
http://geoportale.regione.calabria.it/opendata

- Citta Metropolitana di Napoli: DTM 1 m
http://sit.cittametropolitana.na.it/lidar.html

- Regione Autonoma Friuli Venezia Giulia: DTM 10 m
http://irdat.regione.fvg.it/CTRN/ricerca-cartografia/

- Regione Lazio: DTM 5 m
http://dati.lazio.it/weblist/cartografia/prodotti/2002_2003_DTM_5mt_DXF/

- Regione Liguria: Modello Digitale del Terreno da CTR 1:5000 dal 2007 II ed. 3D/DB (= DTM 5 m)
http://www.regione.liguria.it/sep-servizi-online/catalogo-servizi-online/opendata/item/7192-dtm-modello-digitale-del-terreno-da-ctr-15000-dal-2007-ii-ed-3ddb-topografico-passo-5-m.html

- Regione Lombardia: DTM5x5 Modello Digitale del Terreno 2015 (= DTM 5 m)
http://www.geoportale.regione.lombardia.it/en/download-ricerca

- Regione Piemonte: RIPRESA AEREA ICE 2009-2011 - DTM (= DTM 5 m)
http://www.geoportale.piemonte.it/geocatalogorp/index.jsp

- Regione Puglia: DTM 8 m
http://www.sit.puglia.it/portal/portale_cartografie_tecniche_tematiche/Download/Cartografie

- Regione Autonoma della Sardegna, Sardegna Geoportale: DTM 1 m and DTM 10 m
http://www.sardegnageoportale.it/webgis2/sardegnamappe/?map=download_raster
DTM 1m, DTM MATTM 1m - Aree critiche, DTM MATTM 1m -  Costa Gallura, DTM MATTM 1m - Fasce fluviali, DTM 10m (digitized DTM based on a Topomap with 10 m contour lines)

- Regione Siciliana: MDT 2012-2013 2x2 ETRS89  (= DTM 2 m)
http://www.sitr.regione.sicilia.it/geoportalen/sfoglia-il-catalogo/

- Regione Toscana: DTM 10 m
http://dati.toscana.it/dataset/dem10mt

- Provincia Autonoma di Trento: LIDAR rilievo 2006/2007/2008 (= DTM 2 m)
http://dati.trentino.it/dataset/lidar-rilievo-2006-2007-2008-link-al-servizio-di-download

- Autonome Provinz Bozen-S�dtirol: DTM 5 m
http://www.provinz.bz.it/natur-raum/themen/landeskartografie-download.asp

- Regione Autonoma Valle d'Aosta: DTM 2005/2008 aggregato (voli laser scanner 2005 e 2008), intero territorio regionale (= DTM 2 m)
http://geoportale.partout.it/prodotti_cartografici/repertorio_cartografico/dtm/default_i.aspx

- Regione Veneto: DTM regionale con celle di 5 metri di lato (= DTM 5 m)
http://dati.veneto.it/dataset/dtm-regionale-con-celle-di-5-metri-di-lato

- Malta Planning Authority: DTM 10 m
https://www.pa.org.mt/

- Viewfinder Panoramas by Jonathan de Ferranti: 1" DTMs
http://viewfinderpanoramas.org/dem3.html
Jonathan de Ferranti permitted me to use his elevation data within my DTM using the CC BY 4.0 license terms.

- NASA: SRTM Version 3.0 Global 1" DTMs
http://dwtkns.com/srtm30m/
