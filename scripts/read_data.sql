SELECT * from sites_iliad;



-- count amount of different arfcn_codes
SELECT count(arfcn) as count_arfcn, arfcn from sites_iliad
GROUP BY arfcn
ORDER BY count_arfcn DESC;


SELECT node_id, count(node_id) as node_count, max(cell_lat) as maxlat, min(cell_lat) as minlat, max(cell_long) as maxlon, min(cell_long) as minlon from sites_iliad
GROUP BY node_id;


SELECT node_id, max(cell_lat) as maxlat, avg(cell_lat) as avglat, min(cell_lat) as minlat, max(cell_lat) = avg(cell_lat) from sites_iliad
GROUP BY node_id;

SELECT node_id, min(cell_lat) as cell_lat, min(cell_long) as cell_long from sites_iliad
GROUP BY node_id;