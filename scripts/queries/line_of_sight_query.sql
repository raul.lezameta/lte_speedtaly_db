SELECT st_union(rast)
FROM dtmf
WHERE ST_Intersects(
              rast,
              ST_SetSRID(ST_MakeLine(St_MakePoint(661570, 5171892), St_MakePoint(667496, 5172964)), 3044)
          );


SELECT *
FROM dtm
WHERE ST_Intersects(
              rast,
              ST_SetSRID(ST_MakeLine(St_MakePoint(661570, 5171892), St_MakePoint(667496, 5172964)), 3044)
          );

SELECT st_asewkt(ST_SetSRID(ST_MakeLine(St_MakePoint(661570, 5171892), St_MakePoint(667496, 5172964)), 3044));

SELECT st_asewkt(rast)
FROM dtm;


SELECT st_value(rast, ST_SetSRID(ST_MakeLine(St_MakePoint(661570, 5171892), St_MakePoint(667496, 5172964)), 3044))
FROM dtm

SELECT st_makeline()
FROM dtm,
     bts;


SELECT st_asewkt(st_makeline(b1.geom, b2.geom))
FROM bts b1,
     bts b2
WHERE b1.id = 228094
  AND b2.id = 228008;


SELECT ST_intersects(rast, geomfromewkt(
        'SRID=3044;LINESTRING(661570.182691923 5171891.80658851, 665578.425307009 5171451.01705581)')) i
FROM dtm
ORDER BY i DESC;

SELECT *
FROM dtm;


SELECT line_of_sight(
               ST_SetSrid(St_MakePoint(665413,5167719), 3044) ,
               ST_SetSrid(St_MakePoint(657495,5170104), 3044)
           );


SELECT line_of_sight(
               ST_SetSrid(St_MakePoint(665413,5167719), 3044) ,
               ST_SetSrid(St_MakePoint(666680,5165280), 3044)
           );
SELECT st_srid(rast) from dtmf LIMIT 1;

SELECT ST_SetSrid(St_MakePoint(661570.182691923, 5171891.80658851), 3044)::geography;

SELECT st_asewkt(st_transform(st_setsrid(st_makepoint(664568, 5165988),3044), 4326));


SELECT st_transform(ST_SetSrid(St_MakePoint(661570.182691923, 5171891.80658851), 3044), 4326);
-- Line of Sight Function
