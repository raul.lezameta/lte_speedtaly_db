
SELECT bts_as_geojson('WindTre', 40.87614141141369, 14.6173095703125);



SELECT st_value(rast, st_transform(st_setsrid(st_makepoint(40.87614141141369, 14.6173095703125),4326), 3044))
from dtmf
where st_intersects(rast, st_transform(st_setsrid(st_makepoint(40.87614141141369, 14.6173095703125),4326), 3044));


SELECT st_asewkt(st_transform(st_setsrid(st_point(14.6173095703125, 40.87614141141369),4326), 3044));

SELECT st_srid(rast) from dtmf limit 1;

SELECT b
FROM (SELECT JSON_ARRAY_ELEMENTS_TEXT(arfcn)::int
      FROM bts
      WHERE bts.id = 228315) AS b;


-- TODO: change Formula!!! -> go on here
-- TODO: speed as json with arfcn as value
SELECT d1.arfcn,
       band,
       frequency_mhz,
       max_speed_mbits,
       max_range,
       (1 - 1 / max_range) * spectrum.max_speed_mbits AS actual_speed
FROM (
         SELECT JSON_ARRAY_ELEMENTS_TEXT(arfcn)::integer AS arfcn
         FROM bts
         WHERE bts.id = 228315
     ) AS d1
         INNER JOIN spectrum ON d1.arfcn = spectrum.earfcn;

SELECT d1.arfcn, ROW_TO_JSON(j)
FROM (
         SELECT JSON_ARRAY_ELEMENTS_TEXT(arfcn)::integer AS arfcn
         FROM bts
         WHERE bts.id = 228315
     ) AS d1
         INNER JOIN spectrum ON d1.arfcn = spectrum.earfcn,
     (SELECT d1.band, d1.max_range FROM d1) j;

-- TODO: VERY PROMISING!!!
SELECT JSON_BUILD_OBJECT(earfcn, ROW_TO_JSON(j)) AS vl
FROM spectrum,
     (SELECT max_range, max_speed_mbits FROM spectrum) j;

SELECT JSON_BUILD_OBJECT(d2.arfcn, ROW_TO_JSON(arfcn, d2.band))
FROM (
         SELECT arfcn, band
         FROM (
                  SELECT JSON_ARRAY_ELEMENTS_TEXT(arfcn)::INTEGER AS arfcn
                  FROM bts
                  WHERE bts.id = 228315
              ) AS d1
                  INNER JOIN spectrum ON d1.arfcn = spectrum.earfcn
     ) AS d2
;

SELECT JSON_BUILD_OBJECT(d2.arfcn, max_speed_mbits)
FROM (
         SELECT *
         FROM (
                  SELECT JSON_ARRAY_ELEMENTS_TEXT(arfcn)::INTEGER AS arfcn
                  FROM bts
                  WHERE bts.id = 228315
              ) AS d1
                  INNER JOIN spectrum ON d1.arfcn = spectrum.earfcn
     ) AS d2;


SELECT d1.bnd
FROM (
         SELECT JSON_ARRAY_ELEMENTS_TEXT(bands)::integer AS bnd
         FROM bts
         WHERE bts.id = 228315) AS d1;


SELECT JSON_ARRAY_ELEMENTS_TEXT(bands)::int AS bnd
FROM bts

WHERE bts.id = 228315
;


DROP FUNCTION bts_as_geojson;

SELECT max_range * 1000
FROM spectrum
ORDER BY max_range DESC
LIMIT 1;


SELECT st_transform(st_setsrid(st_makepoint(11.35, 46.49, 200), 4326), 3044);

SELECT st_value(dtm.rast, st_transform(st_setsrid(st_point(11.35, 46.49), 4326), 3044)) z
FROM dtm
WHERE st_intersects(rast, st_transform(st_setsrid(st_point(11.35, 46.49), 4326), 3044));

SELECT st_asewkt(st_transform(st_setsrid(st_point(11.35, 46.49), 4326), 3044));

SELECT bts_as_geojson('WindTre');
SELECT bts_as_geojson('Iliad');

CREATE OR REPLACE FUNCTION test_func(id = integer)
    LANGUAGE plpgsql
AS
$$
DECLARE
    collection;
BEGIN

END;

$$;


-- TODO: extend to include all


SELECT st_asgeojson(st_transform(geom, 4326))
FROM bts
LIMIT 10;



SELECT ROW_TO_JSON(fc)
FROM (SELECT 'FeatureCollection' AS type, ARRAY_TO_JSON(ARRAY_AGG(f)) AS features
      FROM (SELECT 'Feature' AS type, ST_AsGeoJSON(lg.geom)::json AS geometry, ROW_TO_JSON((id, name)) AS properties
            FROM cambridge_coffee_shops AS lg
            WHERE lg.name = 'name' AS f) AS fc;


SELECT