-- CREATE DATABASE "lte_speedtaly";
-- CREATE EXTENSION postgis;
-- CREATE EXTENSION postgis_raster;

DROP TABLE IF EXISTS sites_Iliad;
DROP TABLE IF EXISTS sites_WindTre;
DROP TABLE IF EXISTS sites_Vodafone;
DROP TABLE IF EXISTS sites_TIM;

CREATE TABLE sites_Iliad
(
    id        serial
        CONSTRAINT sites_Iliad_pk
            PRIMARY KEY,
    tech      int,
    mcc       int,
    mnc       int,
    lac_tac   int,
    node_id   int,
    cid       int,
    psc_pci   int,
    band      varchar(50),
    arfcn     int,
    site_name varchar(100),
    cell_lat  double precision,
    cell_long double precision,
    cell_name varchar(100),
    azimuth   double precision,
    height    double precision,
    tilt_mech varchar(50),
    tilt_el   varchar(50)
);

CREATE TABLE sites_Vodafone
(
    id        serial
        CONSTRAINT sites_Vodafone_pk
            PRIMARY KEY,
    tech      int,
    mcc       int,
    mnc       int,
    lac_tac   int,
    node_id   int,
    cid       int,
    psc_pci   int,
    band      varchar(50),
    arfcn     int,
    site_name varchar(100),
    cell_lat  double precision,
    cell_long double precision,
    cell_name varchar(100),
    azimuth   double precision,
    height    double precision,
    tilt_mech varchar(50),
    tilt_el   varchar(50)
);

DROP TABLE IF EXISTS sites_tim;
CREATE TABLE sites_TIM
(
    id        serial
        CONSTRAINT sites_TIM_pk
            PRIMARY KEY,
    tech      int,
    mcc       int,
    mnc       int,
    lac_tac   int,
    node_id   int,
    cid       int,
    psc_pci   int,
    band      varchar(50),
    arfcn     int,
    site_name varchar(100),
    cell_lat  double precision,
    cell_long double precision,
    cell_name varchar(100),
    azimuth   double precision,
    height    double precision,
    tilt_mech varchar(50),
    tilt_el   varchar(50)
);

CREATE TABLE sites_WindTre
(
    id        serial
        CONSTRAINT sites_WindTre_pk
            PRIMARY KEY,
    tech      int,
    mcc       int,
    mnc       int,
    lac_tac   int,
    node_id   int,
    cid       int,
    psc_pci   int,
    band      varchar(50),
    arfcn     int,
    site_name varchar(100),
    cell_lat  double precision,
    cell_long double precision,
    cell_name varchar(100),
    azimuth   double precision,
    height    double precision,
    tilt_mech varchar(50),
    tilt_el   varchar(50)
);

