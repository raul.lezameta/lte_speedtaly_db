CREATE OR REPLACE FUNCTION line_of_sight(IN startloc geometry,
                                         IN endloc geometry
) RETURNS BOOLEAN AS
$BODY$
    -- This function calculates if there is a line of sight between 2 points.
-- It does this by analysing elevation data along the direct path towards
-- the destination point.
-- The comments in the function describe the process as if a person was standing
-- at the startloc (parameter) and was walking towards the endloc (parameter)
-- the walking process naturally involves steps. These steps are similar to
-- what the function does. It starts by finding the elevation at both the startloc
-- and the endloc and then calculates the pitch/angle of the line of sight.
-- The function then enters a loop, this is the walking loop, where we start
-- taking steps towards the endloc. After every step we calculate the pitch from
-- the starting location to the end location. If this pitch is ever higher or equal
-- to the pitch of the endloc then we know we cannot see the endloc from the startloc
-- as it's being obscured by our current location.
--
-- The function calculates these pitches using trig functions then it takes that
-- angle which has been calculated and subtracts the number of degrees around the
-- world that the object is away. It does this over a fixed sized sphere rather than
-- a complex spheoid. Please note that at this time atmospheric refraction is not
-- taken into account, this means that objects on the horizon could be miscalculated
-- by around half a degree! This means mountains in the distance will actually be
-- visibly slightly higher than they will be said to be by this function.

DECLARE
    elevationdata           RASTER;
    DECLARE tot_distance    DOUBLE PRECISION;
    DECLARE cur_distance    DOUBLE PRECISION;
    DECLARE bearing         DOUBLE PRECISION;
    DECLARE start_elevation DOUBLE PRECISION;
    DECLARE end_elevation   DOUBLE PRECISION;
    DECLARE cur_elevation   DOUBLE PRECISION;
    DECLARE step_size       DOUBLE PRECISION;
    DECLARE curloc          GEOGRAPHY;
    DECLARE end_pitch       DOUBLE PRECISION;
    DECLARE cur_pitch       DOUBLE PRECISION;

BEGIN
    -- This query builds a raster which contains all of the raster data
    -- between 2 points. We use ST_Makeline to create a line between our
    -- starting point and our destination. It is quite possible that we'll
    -- find no or just partial raster data between our 2 points. Later we
    -- do test to see if we got some and return NULL if we found no data.

    startloc := st_transform(startloc, 4326);
    endloc := st_transform(endloc, 4326);

    RAISE NOTICE 'startloc: %; dtype: %', st_asewkt(startloc), pg_typeof(startloc);
    RAISE NOTICE 'endloc: %; dtype: %', st_asewkt(endloc), pg_typeof(endloc);

    RAISE NOTICE 'linestring in 3044: %', st_asewkt(st_transform(ST_Makeline(CAST(startloc AS GEOMETRY), CAST(endloc AS GEOMETRY)), 3044));

    SELECT ST_UNION(rast)
    INTO elevationdata
    FROM dtmf e
    WHERE ST_Intersects(rast, st_transform(ST_Makeline(CAST(startloc AS GEOMETRY), CAST(endloc AS GEOMETRY)), 3044));
    RAISE NOTICE 'elevationdata fund';
    -- If we found no data at all then we can quit... At this
    -- point we have no idea if there is a line of sight, so we
    -- return NULL.

    IF elevationdata IS NULL THEN
        RAISE NOTICE 'No elevation data found.';
        RETURN NULL;
    END IF;

    -- We now set the elevation of our start point and our end point.
    -- Note that there currently is a bit of a fuzz factor here and I'm
    -- adding 2 metres to both these values. This is because at least for
    -- our start value our eyes are above the ground and not on the ground,
    -- so we'll have slightly more things in sight. For the end elevation
    -- this is not quite the case but the 2 metres was added due to the
    -- shapes of some mountains. If for example the mountain is completely
    -- flat at the top and we're standing in a location lower than it, if
    -- the summit location is marked in the middle of that flat area then
    -- we'll not be able to see it. I did not find this ideal as in reality
    -- I could see the mountain, just maybe not quite the middle of the summit,
    -- so the 2 meters being added to the end_elevation is fuzz factor, it
    -- perhaps should be more complex and be happy enough with seeing a point
    -- within X number of meters of the summit. At the time of writing this
    -- I could not decide what that value should be, so it remains like this.

    start_elevation := ST_Value(elevationdata, st_transform(CAST(startloc AS GEOMETRY), 3044)) + 2 + 50;
    end_elevation := ST_Value(elevationdata, st_transform(CAST(endloc AS GEOMETRY), 3044)) + 120; -- probably not icorrect

    RAISE NOTICE 'start elevation: %, end_elevation: %', start_elevation, end_elevation;

    -- Now calculate the bearing which we must "walk" to
    -- our far off destination.
    bearing := ST_Azimuth(startloc, endloc);

    RAISE NOTICE 'bearing: %', bearing;

    -- A variable to save the total distance we must walk.
    tot_distance := ST_Distance(st_transform(startloc, 3044), st_transform(endloc, 3044));

    RAISE NOTICE 'tot_distance: %', tot_distance;
    -- Set our step size, smaller steps will mean more loops and slower to calculate.
    -- Also there is no point in going in smaller steps than the detail of the raster.
    -- This should match the raster resolution or be more than it for if performance
    -- is a problem.
    step_size = 30;
    -- metres

    -- We must now work out the pitch in degrees of our line of
    -- sight from our current location to the destination.
    -- Here we use atan which will give us a pitch, or angle on a triangle, since
    -- the earth is round we need to reduce the pitch by the number of degrees
    -- around the earth that the object is. We use a fixed radius for this which
    -- is not quite accurate but it will do for now. Variations caused by
    -- Atmospheric Refraction will likely cause much more variation than the shape
    -- of the planet.

    end_pitch := DEGREES(ATAN((end_elevation - start_elevation) / tot_distance)) -
                 (tot_distance / (6370986 * PI() * 2) * 360);
    RAISE NOTICE 'end_pitch: %', end_pitch;
    -- We now start a loop to walk to our destination.
    -- Note that we stop checking the distance when we're
    -- within step_size to the object, as there's no point in
    -- checking if we can see the object when we're standing
    -- on top of it.

    -- First we just need to take our first step...

    cur_distance := step_size;

    WHILE cur_distance <= (tot_distance - step_size)
        LOOP
        -- Now work out the location of our new step based on
        -- our starting location, the current distance we've
        -- travelled and the bearing to the destination.

            curloc := ST_Project(startloc, cur_distance, bearing);
            RAISE NOTICE 'curloc: %; dtype: %', st_asewkt(curloc), pg_typeof(curloc);

            -- Now let's look at the elevation of the current location.
            cur_elevation := ST_Value(elevationdata, st_transform(CAST(curloc AS GEOMETRY),3044));
            RAISE NOTICE 'cur_elevation: %; dtype: %', cur_elevation, pg_typeof(cur_elevation);

            RAISE NOTICE 'start_elevation = %, end_elevation = %, cur_elevation = %, cur_distance = %, bearing = %',                   start_elevation, end_elevation, cur_elevation, cur_distance, degrees(bearing);

            -- Calculate the pitch to our current location, same as we did for
            -- the destination before the loop began.
            cur_pitch := DEGREES(ATAN((cur_elevation - start_elevation) / cur_distance)) -
                         (cur_distance / (6370986 * PI() * 2) * 360);
            RAISE NOTICE 'cur_pitch: %; dtype: %', cur_pitch, pg_typeof(cur_pitch);

            -- Now we simply check if the pitch to from our starting
            -- point to our current location is greater or equal to
            -- the pitch we calculated from the start point to the end
            -- point. If it is then we can't see the end location due
            -- to the current point appearing to be taller from the
            -- start point.

            IF cur_pitch >= end_pitch THEN
                RAISE NOTICE 'Cannot see over object at pitch %. Pitch to destination is %, start elevation %, object elevation = %, destination elevation = %, dist from start = %, dist from end = %',
                    cur_pitch, end_pitch, start_elevation, cur_elevation, end_elevation, cur_distance, tot_distance - cur_distance;
                RAISE NOTICE 'no line of sight';
                RETURN FALSE;
            END IF;

            -- Now we can take another step then start do the whole process again...
            -- That is, providing we've not arrived at our destination yet.
            cur_distance := cur_distance + step_size;
        END LOOP;
    RETURN TRUE;
END;
$BODY$
    LANGUAGE plpgsql STABLE
                     COST 1000;