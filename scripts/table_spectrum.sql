DROP TABLE IF EXISTS spectrum;

CREATE TABLE spectrum
(
    earfcn          int              NOT NULL
        CONSTRAINT spectrum_pk
            PRIMARY KEY,
    provider        varchar(50)      NOT NULL,
    band            int              NOT NULL,
    frequency_mhz   int              NOT NULL,
    bandwith_mhz    int              NOT NULL,
    max_speed_mbits int              NOT NULL,
    coverage_area   double precision NOT NULL
);

-- https://halberdbastion.com/technology/cellular/4g-lte/lte-frequency-bands FIXME: possibly wrong coverage areas
INSERT INTO public.spectrum (earfcn, provider, band, frequency_mhz, bandwith_mhz, max_speed_mbits, coverage_area)
VALUES
    -- Band 20
    (6200, 'WindTre', 20, 800, 10, 100, 147.40),
    (6300, 'TIM', 20, 800, 10, 100, 147.40),
    (6400, 'Vodafone', 20, 800, 10, 100, 147.40),
    -- Band 8
    (3526, 'TIM', 8, 900, 5, 50, 116.60),
    (3676, 'Vodafone', 8, 900, 5, 50, 116.60),
    -- Band 32
    (10020, 'TIM', 32, 1500, 20, 200, 108.20),      -- possibly wrong!!
    (10220, 'Vodafone', 32, 1500, 20, 200, 108.20), -- possibly wrong!!
    -- Band 3
    (1350, 'TIM', 3, 1800, 20, 200, 108.40),
    (1500, 'Iliad', 3, 1800, 10, 100, 108.40),
    (1650, 'WindTre', 3, 1800, 20, 200, 108.40),
    (1850, 'Vodafone', 3, 1800, 20, 200, 108.40),
    -- Band 1
    (150, 'WindTre', 1, 2100, 10, 100, 105.10),
    (400, 'Iliad', 1, 2100, 10, 100, 105.10),
    (501, 'Vodafone', 1, 2100, 10, 100, 105.10),
    --- Band 1 alternative configuration
    (300, 'TIM', 1, 2100, 10, 100, 105.10),
    (525, 'Vodafone', 1, 2100, 15, 150, 105.10),
    -- Band 7
    (2900, 'Iliad', 7, 2600, 10, 100, 96.90),
    (3025, 'Vodafone', 7, 2600, 15, 150, 96.90),
    (3175, 'TIM', 7, 2600, 15, 150, 96.90),
    (3350, 'WindTre', 7, 2600, 20, 200, 96.90),
    -- Band 7 alternative configuration
    (2800, 'Iliad', 7, 2600, 10, 100, 96.90),
    -- Band 38
    (37900, 'WindTre', 38, 2600, 20, 150, 96.90) -- possibly wrong!!
;

ALTER TABLE spectrum
    ADD max_range double precision;

UPDATE spectrum
SET max_range = round(SQRT(coverage_area / PI())::numeric, 2)
WHERE max_range ISNULL;