DROP TABLE IF EXISTS bts;

CREATE TABLE bts
(
    id        int
        CONSTRAINT bts_pk
            PRIMARY KEY,
    site_name varchar(100),
    lat       double precision,
    lon       double precision,
    z         double precision,
    MNO       varchar(50),
    arfcn     json,
    geom      geometry(PointZ, 3044)
--     geom  geometry(PointZ, 4326),
);

INSERT INTO bts
SELECT node_id,
       MIN(site_name)                           AS site_name,
       MIN(cell_lat)                            AS cell_lat,
       MIN(cell_long)                           AS cell_long,
       NULL,
       'Iliad',
       ARRAY_TO_JSON(ARRAY_AGG(DISTINCT arfcn)) AS arfcn,
       NULL
FROM sites_iliad
WHERE band IS NOT NULL
GROUP BY node_id;


INSERT INTO bts
SELECT node_id,
       MIN(site_name)                           AS site_name,
       MIN(cell_lat)                            AS cell_lat,
       MIN(cell_long)                           AS cell_long,
       NULL,
       'WindTre',
       ARRAY_TO_JSON(ARRAY_AGG(DISTINCT arfcn)) AS arfcn,
       NULL
FROM sites_windtre
WHERE band IS NOT NULL
GROUP BY node_id;

INSERT INTO bts
SELECT node_id,
       MIN(site_name)                          AS site_name,
       MIN(cell_lat)                           AS cell_lat,
       MIN(cell_long)                          AS cell_long,
       NULL,
       'TIM',
       ARRAY_TO_JSON(ARRAY_AGG(DISTINCT arfcn)) AS arfcn,
       NULL
FROM sites_tim
WHERE band IS NOT NULL
GROUP BY node_id
ON CONFLICT DO NOTHING;;

INSERT INTO bts
SELECT node_id,
       MIN(site_name)                          AS site_name,
       MIN(cell_lat)                           AS cell_lat,
       MIN(cell_long)                          AS cell_long,
       NULL,
       'Vodafone',
       ARRAY_TO_JSON(ARRAY_AGG(DISTINCT arfcn)) AS arfcn,
       NULL
FROM sites_vodafone
WHERE band IS NOT NULL
GROUP BY node_id;


UPDATE bts
SET geom = st_transform(st_setsrid(st_makepoint(lon, lat, 0), 4326), 3044)
WHERE geom IS NULL;
