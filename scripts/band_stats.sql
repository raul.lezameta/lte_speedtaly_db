CREATE OR REPLACE FUNCTION band_stats(sel_id int, dist double precision) RETURNS json
    LANGUAGE plpgsql
AS
$$
DECLARE
    result json;
BEGIN
    SELECT JSON_AGG(
                   JSON_BUILD_OBJECT(
                           'arfcn', arfcn,
                           'band', band,
                           'frequency_mhz', frequency_mhz,
                           'max_speed', max_speed_mbits,
                           'max_range', max_range,
                           'actual_speed',
                           ((max_range - dist) > 0)::int * round(
                           (max_range - dist) / max_range * max_speed_mbits)
                       )
               )
    INTO result
    FROM (
             SELECT JSON_ARRAY_ELEMENTS_TEXT(arfcn)::int arfcn
             FROM bts
             WHERE id = sel_id
         ) AS b2
             INNER JOIN spectrum ON b2.arfcn = spectrum.earfcn;

    RETURN result;
END;
$$;