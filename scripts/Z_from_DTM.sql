-- Update z for every bts location

UPDATE bts b -- 1:25 min
SET z    = st_value(d.rast, b.geom)
--     geom = st_setsrid(st_makepoint(st_x(b.geom), st_y(b.geom), z), 3044)
FROM dtmf d
WHERE st_intersects(d.rast, b.geom);

UPDATE bts
SET geom = st_setsrid(st_makepoint(st_x(geom), st_y(geom), z), 3044)
WHERE z NOTNULL ;