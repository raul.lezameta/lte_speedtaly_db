-- FUNCTION get bands from id
CREATE OR REPLACE FUNCTION bands_from_id(sel_id int) RETURNS json
    LANGUAGE plpgsql
AS
$$
DECLARE
    result json;
BEGIN

    SELECT ARRAY_TO_JSON(ARRAY_AGG(band::int)) AS bands -- FIXME: remove COVID 2600 Iliad
    INTO result
    FROM (
             SELECT DISTINCT band
             FROM sites_windtre -- TODO: extend to work with all mno's
             WHERE node_id = sel_id
         ) AS f;

    RETURN result;

END;
$$;


SELECT bands_from_id(102006);


SELECT bands
FROM bts
WHERE id = 102006;

drop FUNCTION if
    EXISTS bts_as_geojson;

-- Function bts_as_geojson
CREATE OR REPLACE FUNCTION bts_as_geojson(sel_mno varchar(50), lat double precision, lon double precision) RETURNS json
    LANGUAGE plpgsql
AS
$$
DECLARE
    collection json;
    pos_2d     geometry;
    z          double precision;
    pos_3d     geometry;
    max_dist   double precision;
    result json;
BEGIN
    pos_2d := st_transform(st_setsrid(st_point(lon, lat), 4326), 3044);

    SELECT st_value(dtmf.rast, pos_2d)
    INTO z
    FROM dtmf
    WHERE st_intersects(dtmf.rast, pos_2d);


    pos_3d := st_setsrid(st_makepoint(st_x(pos_2d), st_y(pos_2d), z), 3044);

    SELECT max_range * 1000
    INTO max_dist
    FROM spectrum
    ORDER BY max_range
        DESC
    LIMIT 1;

    SELECT ROW_TO_JSON(fc)
    INTO collection
    FROM (
             SELECT 'FeatureCollection' AS type, ARRAY_TO_JSON(ARRAY_AGG(f)) AS features
             FROM (
                      SELECT 'Feature'                                                                   AS type,
                             ST_AsGeoJSON(st_transform(lg.geom, 4326))::json                             AS geometry,
                             JSON_BUILD_OBJECT('id', lg.id,
                                               'name', lg.site_name,
                                               'mno', lg.mno,
                                               'dist', st_distance(lg.geom, pos_3d),
                                               'visible', line_of_sight(pos_2d, lg.geom),
                                               'arfcn', lg.arfcn,
                                               'bands', band_stats(lg.id, st_distance(lg.geom, pos_3d)/1000)) AS properties
                      FROM bts
                               AS lg
                      WHERE lg.z NOTNULL
                        AND lg.mno = sel_mno
                        AND st_distance(lg.geom, pos_3d) < max_dist
                      ORDER BY st_distance(lg.geom, pos_3d)
                      LIMIT 15
                  ) AS f
         ) AS fc;

    result := json_build_object('bts_geojson', collection, 'z', z);
    RAISE NOTICE 'ok';

    RETURN result;
END;
$$;