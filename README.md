# lte_speedtally_db

This repository contains the code to build a PostGIS database which can be used on combination with https://gitlab.com/raul.lezameta/lte_speedtaly to build a RESTful API to calculate mobile network speeds in Italy.

The used data comes from https://lteitaly.it/

> Note: This repository must be used in combination with https://gitlab.com/raul.lezameta/lte_speedtaly/, which provides the code to build the associated RESTful API.

# Getting Started:

1. Modify `docker-compoes.yml` and set desired username, password and port of the PostGIS server
```yaml
...
environment:
    - POSTGRES_USER=...
    - POSTGRES_PASS=...
...
ports:
    - "...:5432" # port mapping 5432 => ...
```
2. Use `docker-compoes.yml` to create and run the PostGIS database as a Docker Container
3. Execute the follwing steps on the database:
   1. Run `scripts/create_db.sql`
   2. Load data into db:
        * Load newest iliad, tim, vodafone, windtre csv files (in data folder) into according tables (sites_iliad, sites_tim, etc.)
   3. Run `scripts/bts_generate.sql`
   4. Create DTM table; run `data/elev_full.sql`
   5. Run `scripts/Z_from_DTM.sql`
   6. Run `scripts/table_spectrum.sql`
   7. Run `scripts/line_of_sight.sql`
   8. Run `scripts/band_stats.sql`
   9.  Run `scripts/bts_as_gejson.sql`

> Note: PostGIS database can also be created manually (skip steps 1,2)